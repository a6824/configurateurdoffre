require('dotenv').config()

let host = process.env.HOST || "localhost"
let user = process.env.USER || "root"
let password = process.env.PASSWORD || ""
let db = process.env.DB || "configoffre"
let dialect = process.env.dialect || "mysql"

module.exports = {
    HOST: host,
    USER: user,
    PASSWORD: password,
    DB: db,
    dialect: dialect,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};