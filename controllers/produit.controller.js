//dépendances
const db = require("../models");

//fonctions pour les produits
exports.getAll = async (req, res) => {
    let produits = await db.produit.findAll();
    res.status(200).json(produits)
}

exports.getAllWeb = async (req, res) => {
    let produits = await db.produit.findAll();
    res.render("pages/products/products", {products: produits})
}

exports.getById = async (req, res) => {
    let id = req.params.id;
    let produit = await db.produit.findOne({
        where: {
            id: id
        }
    })
    res.status(200).json(produit)
}

exports.createProduit = async (req, res) => {
    const {
        nom
    } = req.body;
    await db.produit.create({
        nom: nom
    })
        .then(produit => res.json({
            produit,
            msg: 'specialite created succesfully'
        }));
}

exports.createProduitWeb = async (req, res) => {
    const {
        nom,
        prix
    } = req.body;
    await db.produit.create({
        nom: nom,
        prix: prix
    })
    await db.produit.findAll().then(produits => {
        res.render("pages/products/products", {products: produits})
    })
}

exports.updateProduit = async (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        })
    }

    await db.produit.update(
        req.body, {
            where: {
                id: req.params.id
            }
        }).then(produit => res.json({
        produit,
        msg: 'produit updated succesfully'
    }))
        .catch(res.status(500).send({
            message: "error with the json data"
        }))
}

exports.delete = async (req, res) => {
    await db.produit.destroy({
        where: {
            id: req.params.id
        }
    })
}

exports.deleteProduitWeb = async (req, res) => {
    await db.produit.destroy({
        where: {
            id: req.params.id
        }
    })
    res.status(200).json({msg: "deleted"})
}
