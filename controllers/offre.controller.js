//dépendances
const db = require("../models");

//fonctions pour les offres
exports.getAll = async (req, res) => {
    let offres = await db.offre.findAll();
    res.status(200).json(offres)
}

exports.getAllWeb = async (req, res) => {
    let offres = await db.offre.findAll();
    res.render("pages/offres/offres", {offres: offres})
}

exports.getById = async (req, res) => {
    let id = req.params.id;
    let offre = await db.offre.findOne({
        where: {
            id: id
        }
    })
    res.status(200).json(offre)
}

exports.createOffre = async (req, res) => {
    const {
        nom,
        remise
    } = req.body;
    let code           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < 10; i++ ) {
        code += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    await db.offre.create({
        nom: nom,
        remise: remise,
        code: code
    })
        .then(offre => res.json({
            offre,
            msg: 'offre created succesfully'
        }));
}

exports.createOffreWeb = async (req, res) => {
    const {
        nom,
        remise
    } = req.body;
    let code           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < 10; i++ ) {
        code += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    await db.offre.create({
        nom: nom,
        remise: remise,
        code: code
    })
    await db.offre.findAll().then(offres => {res.render("pages/offres/offres", {offres: offres})})
}

exports.updateOffre = async (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        })
    }

    await db.offre.update(
        req.body, {
            where: {
                id: req.params.id
            }
        }).then(offre => res.json({
        offre,
        msg: 'offre updated succesfully'
    }))
        .catch(res.status(500).send({
            message: "error with the json data"
        }))
}

exports.delete = async (req, res) => {
    await db.offre.destroy({
        where: {
            id: req.params.id
        }
    })
}

exports.deleteOffreWeb = async (req, res) => {
    await db.offre.destroy({
        where: {
            id: req.params.id
        }
    })
    res.status(200).json({msg: "deleted"})
}
