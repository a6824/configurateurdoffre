//dépendances
const db = require("../models");

//fonctions pour les roles
exports.getAll = async (req, res) => {
    let roles = await db.role.findAll({
        attributes: ['id', 'nom']
    });
    res.status(200).json(roles)
}

exports.getById = async (req, res) => {
    let id = req.params.id;
    let role = await db.role.findOne({
        attributes: ['id', 'nom'],
        where: {
            id: id
        }
    })
    res.status(200).json(role)
}

exports.createRole = async (req, res) => {
    const {
        nom
    } = req.body;
    await db.role.create({
            nom: nom
        })
        .then(role => res.json({
            role,
            msg: 'specialite created succesfully'
        }));
}

exports.updateRole = async (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        })
    }

    await db.role.update(
            req.body, {
                where: {
                    id: req.params.id
                }
            }).then(role => res.json({
            role,
            msg: 'Role updated succesfully'
        }))
        .catch(res.status(500).send({
            message: "error with the json data"
        }))
}

exports.delete = async (req, res) => {
    await db.role.destroy({
        where: {
            id: req.params.id
        }
    })
}
