//dépendances
const db = require("../models");

//fonctions pour les images
exports.getAll = async (req, res) => {
    let images = await db.image.findAll({
        attributes: ['id', 'nom']
    });
    res.status(200).json(images)
}

exports.getById = async (req, res) => {
    let id = req.params.id;
    let image = await db.image.findOne({
        where: {
            id: id
        }
    })
    res.status(200).json(image)
}

exports.createImage = async (req, res) => {
    const {
        nom
    } = req.body;
    await db.image.create({
        nom: nom
    })
        .then(image => res.json({
            image,
            msg: 'specialite created succesfully'
        }));
}

exports.updateImage = async (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        })
    }

    await db.image.update(
        req.body, {
            where: {
                id: req.params.id
            }
        }).then(image => res.json({
        image,
        msg: 'image updated succesfully'
    }))
        .catch(res.status(500).send({
            message: "error with the json data"
        }))
}

exports.delete = async (req, res) => {
    await db.image.destroy({
        where: {
            id: req.params.id
        }
    })
}
