const imageController = require("../controllers/image.controller");
const { authJwt, verifySignUp } = require("../middleware");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post("/images",  [authJwt.verifyToken, authJwt.isAdmin, verifySignUp.checkDuplicateUsername], imageController.createImage);

    app.get("/images", imageController.getAll);

    app.get("/images/:id", imageController.getById);

    app.put("/images/:id", [authJwt.verifyToken, authJwt.isAdmin], imageController.updateImage);

    app.delete("/images/:id", [authJwt.verifyToken, authJwt.isAdmin], imageController.delete)

};