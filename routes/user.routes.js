const userController = require("../controllers/user.controller");
const { authJwt, verifySignUp } = require("../middleware");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/test/all", userController.allAccess);

    app.get("/api/test/user", [authJwt.verifyToken],
        userController.userBoard
    );

    app.get("/api/test/admin", [authJwt.verifyToken, authJwt.isAdmin],
        userController.adminBoard
    );
    
    app.post("/api/users",  [verifySignUp.checkDuplicateUsername], userController.createUser);
    app.post("/users",  [verifySignUp.checkDuplicateUsernameWeb], userController.createUserWeb);
    
    app.get("/api/users", userController.getAll);
    
    app.get("/api/users/:id", userController.getById);
    
    app.put("/api/users/:id", [authJwt.verifyToken, authJwt.isAdmin], userController.update);
    
    app.delete("/api/users/:id", [authJwt.verifyToken, authJwt.isAdmin], userController.delete)

};