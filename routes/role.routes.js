const { authJwt, verifySignUp } = require("../middleware");
const roleController = require("../controllers/role.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
    
    app.get("/roles", roleController.getAll);
    
    app.get("/roles/:id", roleController.getById);
    
    app.post("/roles",  [authJwt.verifyToken, authJwt.isAdmin], roleController.createRole);
    
    app.put("/roles/:id", [authJwt.verifyToken, authJwt.isAdmin], roleController.updateRole);
    
    app.delete("/roles/:id", [authJwt.verifyToken, authJwt.isAdmin], roleController.delete)
};
