const offreController = require("../controllers/offre.controller");
const { authJwt, verifySignUp } = require("../middleware");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post("/api/offres",  [authJwt.verifyToken, authJwt.isAdmin, verifySignUp.checkDuplicateUsername], offreController.createOffre);

    app.get("/api/offres", offreController.getAll);

    app.get("/api/offres/:id", offreController.getById);

    app.put("/api/offres/:id", [authJwt.verifyToken, authJwt.isAdmin], offreController.updateOffre);

    app.delete("/api/offres/:id", [authJwt.verifyToken, authJwt.isAdmin], offreController.delete)

};