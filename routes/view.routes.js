const { authJwt, verifySignUp } = require("../middleware");
const produitController = require("../controllers/produit.controller");
const offreController = require("../controllers/offre.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/", authJwt.verifyTokenWeb, function(req, res) {
        res.render('pages/index')
    })

    app.get("/login", function(req, res) {
        res.render('pages/login', {error: 3, message: ""})
    });

    app.get("/register", function(req, res) {
        res.render('pages/register')
    });

    app.get("/produits", authJwt.verifyTokenWeb, produitController.getAllWeb)
    app.get("/produits/create", authJwt.verifyTokenWeb, function(req, res) {
        res.render("pages/products/add")
    })
    app.post("/produits/add", authJwt.verifyTokenWeb, produitController.createProduitWeb)
    app.get("/produits/delete", authJwt.verifyTokenWeb, function(req, res) {
        res.render("pages/products/delete")
    })
    app.delete("/produits/delete/:id", authJwt.verifyTokenWeb, produitController.deleteProduitWeb)
    app.get("/offres", authJwt.verifyTokenWeb, offreController.getAllWeb)
    app.get("/offres/create", authJwt.verifyTokenWeb, function(req, res) {
        res.render("pages/offres/add")
    })
    app.post("/offres/add", authJwt.verifyTokenWeb, offreController.createOffreWeb)
    app.get("/offres/delete", authJwt.verifyTokenWeb, function(req, res) {
        res.render("pages/offres/delete")
    })
    app.delete("/offres/delete/:id", authJwt.verifyTokenWeb, offreController.deleteOffreWeb)

};