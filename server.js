// prérequis
const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const http = require("http")
const cors = require('cors')
require("dotenv").config()

//variables nécessaire
const app = express()
const port = 3000

// chargement des models
const db = require("./models")

// synchronization de la base
db.sequelize.sync()
    .then(() => {
        console.log('successfully sync base')
    })

// set view engine to ejs
app.set('view engine', 'ejs')
app.use(express.static("views/images"))
// enabling cors request
app.use(cors())
// parse application/json
app.use(bodyParser.json())
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}))
// parse cookie
app.use(cookieParser())

// appel des routes api
require('./routes/auth.routes')(app)
require('./routes/user.routes')(app)
require('./routes/role.routes')(app)
require('./routes/image.routes')(app)
require('./routes/offre.routes')(app)
require('./routes/produit.routes')(app)

//appel des routes de vue
require('./routes/view.routes')(app)

//default routes de test
app.get("/api/readinnes", function (req, res) {
    res.json("hello world")
})

//lancement serveur
console.log(`listening on port ${port}`)
http.createServer(app).listen(port)
