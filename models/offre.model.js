module.exports = (sequelize, Sequelize) => {
    const Offre = sequelize.define("offres", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nom: {
            type: Sequelize.STRING
        },
        remise: {
            type: Sequelize.FLOAT
        },
        code: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });

    return Offre;
};