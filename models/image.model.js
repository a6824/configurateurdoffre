module.exports = (sequelize, Sequelize) => {
    const Image = sequelize.define("images", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        image: {
            type: Sequelize.BLOB
        }
    });

    return Image;
};