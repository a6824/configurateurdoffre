module.exports = (sequelize, Sequelize) => {
    const Produit = sequelize.define("produits", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nom: {
            type: Sequelize.STRING
        },
        prix: {
            type: Sequelize.FLOAT
        }
    });

    return Produit;
};